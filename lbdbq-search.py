#!/usr/bin/env python3
# Copyright (C) Intel Corp.  2018.  All Rights Reserved.

# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice (including the
# next paragraph) shall be included in all copies or substantial
# portions of the Software.
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE COPYRIGHT OWNER(S) AND/OR ITS SUPPLIERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#  **********************************************************************/
#  * Authors:
#  *   Clayton Craft <clayton.a.craft@intel.com>
#  **********************************************************************/

import argparse
import click
import os
import re
import subprocess

CACHE_FILE = os.path.expanduser("~/.cache/lbdbq-search.cache")


def parse_lbdbq_output(out, name):
    """ Parse output from lbdbq, return match or None if no match.
    out - output from lbdbq
    name - tuple in format (lastname, firstname, middle initial)
    """
    for line in out:
        fullname = email = None
        parts = line.split('\t')
        # drop the last part of the line, which is location
        parts.pop()
        for part in parts:
            if ',' in part:
                fullname = part
            elif '@' in part:
                email = part
        if not fullname or not email:
            continue
        # Search with middle initial
        if name[2]:
            result = re.match(("%s, %s %s$" % (name[0], name[1],
                                               name[2])).lower(),
                              fullname.lower())
            if result:
                return ("%s %s %s <%s>" % (name[1].capitalize(),
                                           name[2].capitalize(),
                                           name[0].capitalize(), email))
        # Search without middle initial
        else:
            result = re.match(("%s, %s$" % (name[0], name[1])).lower(),
                              fullname.lower())
            if not result:
                # Try again but be more fuzzy in case user didn't specify
                # middle initial when there is one
                result = re.match(("%s, %s\s" % (name[0], name[1])).lower(),
                                  fullname.lower())
            if result:
                return ("%s %s <%s>" % (name[1].capitalize(),
                                        name[0].capitalize(), email))
    return None


def uncache(cache, name):
    """ Remove name from cache.
    cache - list with items in format:
            <email>\t<lastname, firstname>\t<location>
    name - tuple in format (lastname, firstname, middle initial)
    """
    for item in cache:
        r = parse_lbdbq_output([item], name)
        if r:
            cache.remove(item)


def main():
    names = []
    results = []
    cache = []

    ap = argparse.ArgumentParser()
    ap.add_argument('-f', '--file', action='store', default='',
                    help=('File containing list of names, one per line, in '
                          'the format: FIRST LAST'))
    ap.add_argument('-s', '--search', action='store', default='',
                    help=('Name to search for, in format: "FIRST LAST"'))
    ap.add_argument('-m', '--mutt', action='store_true', default=False,
                    help=('Output results into a comma-separated string that '
                          'mutt would understand.'))
    ap.add_argument('-u', '--uncache', action='store_true', default=False,
                    help=('Query all names with lbdbq, and remove from cache '
                          'if they do not return any results from lbdbq.'))

    args = ap.parse_args()
    if args.search:
        names.append(args.search)
    if args.file:
        if not os.path.exists(args.file):
            ap.print_help()
            quit(1)
        with open(args.file) as f:
            for line in f:
                parts = line.rstrip().split(" ")
                if len(parts) > 1:
                    names.append(line.rstrip())
    if not args.search and not args.file:
        ap.print_help()
        quit(1)

    if os.path.exists(CACHE_FILE):
        with open(CACHE_FILE, 'r') as f:
            cache = f.readlines()
        for i in range(len(cache)):
            cache[i] = cache[i].rstrip()

    with click.progressbar(names) as bar:
        for name in bar:
            result = None
            try:
                first, middle, last = name.split(' ')
            except ValueError:
                first, last = name.split(' ')
                middle = None
            # search cache first
            if not args.uncache:
                result = parse_lbdbq_output(cache, (last, first, middle))
            if not result:
                # no hit in cache, query lbdbq
                cmd = ['lbdbq', last]
                try:
                    out = subprocess.check_output(cmd).decode()
                except subprocess.CalledProcessError:
                    continue
                out_lines = out.split('\n')
                # first line is always some "lbdbq: X matches", so remove it
                out_lines.pop(0)
                for line in out_lines:
                    # Line in format:
                    # <email>\t<last, first, middle>\t<location>
                    result = parse_lbdbq_output(out_lines,
                                                (last, first, middle))
                    if not result:
                        if args.uncache:
                            uncache(cache, (last, first, middle))
                        result = ("%s %s <notfound>" % (first, last))
                    else:
                        if ' alias ' in line:
                            line = line.split(' alias ')[0]
                        cache.append(line.rstrip())
            results.append(result)

    if args.mutt:
        print(', '.join(results))
    else:
        print('\n'.join(results))

    # write out cache, even if empty (e.g. lone item was removed with -u)
    with open(CACHE_FILE, 'w') as f:
        f.write('\n'.join(cache))


if __name__ == '__main__':
    main()
